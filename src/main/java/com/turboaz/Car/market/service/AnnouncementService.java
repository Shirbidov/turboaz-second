package com.turboaz.Car.market.service;

import com.turboaz.Car.market.dto.request.AnnouncementRequest;
import com.turboaz.Car.market.repository.AnnouncementRepository;

public interface AnnouncementService {


    void createAnnouncement(AnnouncementRequest announcementRequest);

}
