package com.turboaz.Car.market.service;

import com.turboaz.Car.market.config.ModelMapperConfig;
import com.turboaz.Car.market.domain.Announcement;
import com.turboaz.Car.market.dto.request.AnnouncementRequest;
import com.turboaz.Car.market.repository.AnnouncementRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AnnouncementServiceImpl implements AnnouncementService{

    private final AnnouncementRepository announcementRepository;
    private final ModelMapper modelMapper;

    @Override
    public void createAnnouncement(AnnouncementRequest announcementRequest) {
        Announcement map = modelMapper.map(announcementRequest, Announcement.class);
        announcementRepository.save(map);
    }
}
