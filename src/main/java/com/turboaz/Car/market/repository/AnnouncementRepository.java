package com.turboaz.Car.market.repository;

import com.turboaz.Car.market.domain.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementRepository extends JpaRepository<Announcement ,Long> {

}
