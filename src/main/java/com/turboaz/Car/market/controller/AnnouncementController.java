package com.turboaz.Car.market.controller;

import com.turboaz.Car.market.dto.request.AnnouncementRequest;
import com.turboaz.Car.market.dto.response.AnnouncementResponse;
import com.turboaz.Car.market.service.AnnouncementService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/announcement")

public class AnnouncementController {

    private final AnnouncementService announcementService;


    @PostMapping
    public ResponseEntity<AnnouncementRequest> createAnnouncement(@RequestBody AnnouncementRequest announcementRequest){

        announcementService.createAnnouncement(announcementRequest);

        return ResponseEntity.ok(announcementRequest);

    }

}
