package com.turboaz.Car.market.dto.response;

import com.turboaz.Car.market.domain.Mark;
import com.turboaz.Car.market.domain.Model;
import com.turboaz.Car.market.domain.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class AnnouncementResponse {

    private Long id;

    private String name;

    private Mark mark;

    private Model model;

    private Double price;

    private User user;

    private LocalDateTime updatedTime;

    private Long viewCount;

    private String detail;

    private String pictureURL;

    //todo add AnnouncementInfo entity

}
