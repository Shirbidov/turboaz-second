package com.turboaz.Car.market.dto.response;

import com.turboaz.Car.market.domain.Model;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkResponse {

    private Long id;

    private String name;

    private Model model;




}
