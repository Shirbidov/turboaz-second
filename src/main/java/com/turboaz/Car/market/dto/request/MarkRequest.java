package com.turboaz.Car.market.dto.request;

import com.turboaz.Car.market.domain.Model;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkRequest {

    private String name;

    private Model model;


}
