package com.turboaz.Car.market.dto.request;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {

    private String name;

    private String surname;

    private String email;

    private String phoneNumber;

    private String userRole;

    private boolean isActive;
}
