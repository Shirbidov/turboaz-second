package com.turboaz.Car.market.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "announcement")

public class Announcement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Mark mark;

    private Model model;

    private Double price;

    private User user;

    private LocalDateTime updatedTime;

    private Long viewCount;

    private String detail;

    private String pictureURL;

    //todo add AnnouncementInfo entity

}
